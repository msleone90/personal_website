from flask import render_template, make_response, request, flash
from flask_mail import Mail, Message
from flask_wtf import Form
from wtforms import StringField, TextAreaField, validators
from datetime import datetime

from app import app

@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return render_template('404.html'), 404