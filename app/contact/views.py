from flask import Blueprint, render_template, make_response, request, flash
from flask_mail import Mail, Message
from flask_wtf import Form
from wtforms import StringField, TextAreaField, validators
from datetime import datetime
from app import app, mail

from app.contact.forms import ContactForm

contact = Blueprint('contact', __name__,
                     template_folder='./templates')

@contact.route('/', methods=['GET', 'POST'])
def index():
    form = ContactForm()

    if request.method == 'POST' and form.validate_on_submit():
        msg = Message(subject=request.form['subject'],
                    sender='MichaelLeone.io <' + app.config.get("MAIL_USERNAME") + '>',
                    recipients=["msleone90@gmail.com"])

        # TODO work on template            
        msg.html = render_template('contact_email.html', name=request.form['name'], email=request.form['email'], body=request.form['message'])
        mail.send(msg)

        flash('Email was sent successfully!', 'success')
    
    return render_template('contact.html', now=datetime.utcnow(), form=form)
