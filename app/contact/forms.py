from flask import Flask, render_template, flash, redirect, url_for, request
from flask_wtf import Form, RecaptchaField
from wtforms import StringField, validators, TextAreaField

#Contact route creation go to WTForms Documentation
class ContactForm(Form):
    name = StringField('Name', validators=[validators.InputRequired(message='Name is Required!'), validators.Length(min=1, max=50)])
    email = StringField('Email', 
                        validators=[
                            validators.InputRequired(message='Email is Required!'), 
                            validators.Email("This field requires a valid email address"), 
                            validators.Length(min=1, max=100)
                        ]
    )
    subject = StringField('Subject', 
                          validators=[
                              validators.InputRequired(message='Subject is Required!'), 
                              validators.Length(min=4, max=100)
                            ]
    )
    message = TextAreaField('Message', 
                            validators=[
                                validators.InputRequired(message='Message is Required!')
                            ], 
                            render_kw={
                                "rows": 5
                            }
    )
    recaptcha = RecaptchaField()