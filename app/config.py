import os

class Config(object):
    # SQLAlchemy

    SQLALCHEMY_DATABASE_URI = "sqlite:///C:/Users/Michael/Documents/PythonCode/projects.db"
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Email Server
    
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_USERNAME = os.environ.get('EMAIL_USER')
    MAIL_PASSWORD = os.environ.get('EMAIL_PASS')

    # Recaptcha

    RECAPTCHA_PUBLIC_KEY = os.environ.get('RECAPTCHA_PUBLIC')
    RECAPTCHA_PRIVATE_KEY = os.environ.get('RECAPTCHA_SECRET')