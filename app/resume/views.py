from flask import Blueprint, render_template, make_response, request, flash
from datetime import datetime

resume = Blueprint('resume', __name__, template_folder='../resume/templates')

@resume.route('/resume')
def index():
    return render_template('resume.html', now=datetime.utcnow())