from app import db

# TODO - Import SQLAlchemy and convert class to ORM
class Projects(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)
    url = db.Column(db.String(100), unique=True, nullable=False)
    description = db.Column(db.String(255), nullable=False)
    language = db.Column(db.String(10), nullable=False)
    create_date = db.Column(db.String(50), nullable=False)
    stars = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return "Projects('{name}', '{description}', '{language}', '{create_date}', '{stars}')".format(name=name, description=description, language='language', create_date=create_date, stars=stars)
