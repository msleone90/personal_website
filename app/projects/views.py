from flask import Blueprint, render_template, make_response, request, flash
from datetime import datetime

from app.projects.models import Projects

projects = Blueprint('projects', __name__, template_folder='../projects/templates')

@projects.route('/')
def index():
    projects = Projects.query.all()

    return render_template('projects.html', now=datetime.utcnow(), projects=projects)