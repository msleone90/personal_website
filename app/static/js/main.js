$(document).ready(function() {
    if($("#page").val() == "index"){
        $(".navbar-brand, .nav-link").attr("style", "color: white !important");
    }

    $('.main-container, .project-table, .form-container').addClass('animated fadeInUp');
    $('.title_section, .education_section, .experience_section, .skills_section, .certs_section').addClass('animated fadeInUp');
});

if($("#page").val() == "index"){
    $(window).scroll(function () {
        if ($(window).scrollTop() >= 100) {
            $(".navbar-brand, .nav-link").attr("style", "color: black !important");
        } else {
            $(".navbar-brand, .nav-link").attr("style", "color: white !important");
        }
    });
}
