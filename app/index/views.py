from flask import Blueprint, render_template, make_response, request, flash
from datetime import datetime

from app import app

main = Blueprint('index', __name__, template_folder='../index/templates')

#mail = Mail(app)

@main.route('/')
def index():
    return render_template('index.html', now=datetime.utcnow())