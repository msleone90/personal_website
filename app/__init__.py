from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect
from flask_mail import Mail, Message

from app.config import Config

app = Flask(__name__)
app.config.from_object(Config)

csrf = CSRFProtect(app)
db = SQLAlchemy(app)
mail = Mail(app)

from app import routes

from app.index.views import main
from app.projects.views import projects
from app.resume.views import resume
from app.contact.views import contact

app.register_blueprint(main, url_prefix='/')
app.register_blueprint(projects, url_prefix='/projects')
app.register_blueprint(resume, url_prefix='/resume')
app.register_blueprint(contact, url_prefix='/contact')