from app import app

if __name__ == '__main__':
    app.secret_key = 'gr33nh0us3'
    app.run(debug=True, port=8080)
